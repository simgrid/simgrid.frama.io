;; emacs -q -l ~/minimal_setup.el
(require 'package)
(setq package-user-dir "./emacs.d/elpa/")
(add-to-list 'package-archives '("org" . "http://orgmode.org/elpa/") t)
(add-to-list 'package-archives '("marmalade" . "http://marmalade.org/elpa/") t)
(package-initialize)
(package-install 'org)
;; (unless package-archive-contents    ;; Refresh the packages descriptions
;;   (package-refresh-contents))
;; (setq package-load-list '(all))     ;; List of packages to load
;; (unless (package-installed-p 'org)  ;; Make sure the Org package is
;;   (package-install 'org))           ;; installed, install it if not
;; (package-initialize)                ;; Initialize & Install Package

