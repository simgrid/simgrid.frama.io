#! /usr/bin/perl
  use Text::BibTeX;
  use strict;

  # This script should be edited in the Publications.org chunk, not directly.
  # C-c C-v t to re-tangle the file when you edit it. 
  # The tangled version is used in the other scripts, so modifs will only have effect if you re-tangle the file.

  my($maxyear) = 2018;

  sub uniq {
      my %seen;
      grep !$seen{$_}++, @_;
  }

  sub bibtable {
      my $bibfile = shift;
      my(%count);
      my @cats = ("citeonly", "use", "extend");
      my(%pp) = (
          "extend" => "New developments of SimGrid",
          "use" => "Using SimGrid",
          "citeonly" => "Citing SimGrid"
          );

      foreach my $cat (@cats) {
          foreach my $year (2001..$maxyear) {
              $count{$pp{$cat}}{$year} = 0;
          }
      }

      while (my $entry = new Text::BibTeX::Entry $bibfile) {
          next unless $entry->parse_ok;
          my $year=$entry->get('year');
          my $cat=$entry->get('note');
	  die "Entry '".$entry->get('title')."' has no year!" unless (defined $year);
          die "Entry '".$entry->get('title')."' has no category!" unless (defined $cat);
          $count{$pp{$cat}}{$year}+=1;
      }
      my @years=();
      foreach my $cat (keys %count) {
          @years = (@years, (keys %{$count{$cat}}));
      }
      @years = uniq(sort {$a <=> $b} @years);


      print "|-\n";
      print "| Year |".(join " | ",@years)." | Total |\n";
      print "|-\n";
      foreach my $cat (uniq sort (values %pp)) {
          my @val = ();
          my $sum = 0;
          foreach my $year (@years) {
              push @val, $count{$cat}{$year};
          }
          $sum += $_ for @val;
          print "| $cat |".(join " | ",@val)." | $sum \n";
      }
      print "|-\n";
  }

  sub bibtable_bytype {
      my $bibfile = shift;
      my $format = shift;
      if(!defined($format)) {
	  $format = "org";
      }
      my %cat;
      my %count;
      my @types = ("article", "phdthesis", "inproceedings");
      foreach my $cat (@types, "others") {
          foreach my $year (2000..$maxyear) {
              $cat{$cat}{$year} = 0;
          }
      }

      while (my $entry = new Text::BibTeX::Entry $bibfile) {
          next unless $entry->parse_ok;
          my $year=$entry->get('year');
          my $cat=$entry->type;
          #if(!($cat ~~ @types)) { $cat = "others"; }
          $count{$cat}{$year}+=1;
      }

      my @years=();
      foreach my $cat (keys %count) {
          @years = (@years, (keys %{$count{$cat}}));
      }
      @years = uniq(sort {$a <=> $b} @years);


      my(%pp) = (
          "inproceedings" => "Conference articles",
          "article" => "Journal articles",
          "phdthesis" => "PhD thesis",
          "others" => "Others",
          );
      if($format eq "org") {
	  print "|-\n";
	  print "| Year |".(join " | ",@years)." | Total |\n";
	  print "|-\n";
	  foreach my $cat (@types, "others") {
	      my @val = ();
	      my $sum = 0;
	      foreach my $year (@years) {
		  push @val, $count{$cat}{$year};
	      }
	      $sum += $_ for @val;
	      print "| $pp{$cat} |".(join " | ",@val)." | $sum \n";
	  }
	  print "|-\n";
	  print "| Total |";
	  my $s = 0;
	  foreach my $year (@years) {
	      my $sum = 0;
	      my @val = ();
	      foreach my $cat (@types, "others") {
		  push @val, $count{$cat}{$year};
	      }
	      $sum += $_ for @val;
	      $s += $sum;
	      print " $sum | ";
	  }
	  print " $s |\n";
	  print "|-\n";
      } elsif ($format =~ /.csv$/) {
	  open OUTPUT, "> ".$format or die;
	  print OUTPUT "Type,Year,Number\n";
	  foreach my $cat (@types, "others") {
	      foreach my $year (@years) {
		  print OUTPUT "$pp{$cat},$year,$count{$cat}{$year}\n";
	      }
	  }
	  close OUTPUT;
      }
  }

  sub format_names {
      my $names = shift;
      my @names = split(/ and /, $names);
      return (join ", ",@names);
  }

  sub format_clean {
      my $str = shift;
      $str =~ s/[{}]*//g;
      $str =~ s/"//g; #"
      return $str;
  }

  sub format_links {
      my $entry = shift;
      my @output;
      if(defined($entry->get('pdf'))) {
          push @output, ("[[".$entry->get('pdf')."][PDF]] ");
      } 
      if(defined($entry->get('url'))) {
          push @output, ("[[".$entry->get('url')."][WWW]] ");
      } 
      if(defined($entry->get('doi'))) {
          my $doi = $entry->get('doi');
          push @output, ("[[http://dx.doi.org/$doi][doi:$doi]] ");
      } 
      return @output;
  }

  sub format_journal {
      my $entry = shift;
      my @output=(format_names($entry->get('author')), ". *",$entry->get('title'),
                  "*. /",$entry->get('journal'),"/, ",
                  $entry->get('year'),". ");
      if(defined($entry->get('volume'))) { push @output, .$entry->get('volume').""; }
      if(defined($entry->get('number'))) { push @output, ("(".$entry->get('number').") "); }

      push @output, format_links($entry);
      return format_clean(join "", @output);
  }

  sub format_conf {
      my $entry = shift;
      my @output=(format_names($entry->get('author')), ". *",$entry->get('title'),
                  "*. In /",$entry->get('booktitle'),"/, ",
                  $entry->get('year'),". ");

      push @output, format_links($entry);
      return format_clean(join "", @output);
  }

  sub format_phdthesis {
      my $entry = shift;
      my $type;
      if(defined($entry->get('type'))) { $type = $entry->get('type'); }
      else { 
         $type = $entry->type;
         if($type =~ /phd/) { $type="PhD. thesis. " ; }
         elsif($type =~ /master/) { $type = "MSc. thesis. " ; }
      }
      my @output=(format_names($entry->get('author')), ". *",$entry->get('title'),
                  "*. $type. /",$entry->get('school'),"/, ",
                  $entry->get('year'),". ");

      push @output, format_links($entry);
      return format_clean(join "", @output);
  }

  sub format_techreport {
      my $entry = shift;
      my @output=(format_names($entry->get('author')), ". *",$entry->get('title'),
                  "*. /",$entry->get('institution'),"/, ",
                  $entry->get('year'),". ");
      if(defined($entry->get('type'))) { push @output, ($entry->get('type')." "); }
      if(defined($entry->get('number'))) { push @output, ("N° ".$entry->get('number')." "); }

      push @output, format_links($entry);
      return format_clean(join "", @output);
  }

  sub bibhtml {
      my $bibfile = shift;
      my $include_cat_pat = shift;
      my $include_type_pat = shift;

      # print("Cat\n\t");
      # if(defined($include_cat_pat)) { print(%$include_cat_pat."\n"); }
      # print("Type\n\t");
      # if(defined($include_type_pat)) { print(%$include_type_pat."\n");} 
      
      my %years;
      while (my $entry = new Text::BibTeX::Entry $bibfile) {
          push @{$years{$entry->get('year')}}, $entry;
      }
      
      foreach my $year (sort {$b cmp $a} keys %years) {
          foreach my $entry (@{$years{$year}}) {
              next unless $entry->parse_ok;
              my $cat = $entry->get('note');
	      my $type = $entry->type;
	      # print "--> $cat $type ".($entry->get('year'))."\n";
	      # print "Entry '".$entry->get('title')."' has no category!" unless (defined $cat);
	      # print "Testing cat $$include_cat_pat{$cat}\n";
	      next unless $$include_cat_pat{$cat};
	      # print "Testing type\n";
	      next unless (!defined($include_type_pat) || $$include_type_pat{$type});

              if($type =~ /article/ || $type =~ /incollection/) {
                  print "- ".(format_journal($entry))."\n";
              } elsif($type =~ /inproceedings/) {
                  print "- ".(format_conf($entry))."\n";
              } elsif($type =~ /techreport/) {
                  print "- ".(format_techreport($entry))."\n";
              } elsif($type =~ /phdthesis/ || $type =~ /mastersthesis/) {
                  print "- ".(format_phdthesis($entry))."\n";
              } else {
                  warn "Unknown type ".$type."\n";
              }
          }
      }
  }

  sub count_authors {
      my $bibfile = shift;
      my $include_cat_pat = shift;
      my %authors;
      while (my $entry = new Text::BibTeX::Entry $bibfile) {
          my $cat = $entry->get('note');
          next unless $$include_cat_pat{$cat};
          my $list = $entry->get('author');
#          print "Entry: $list\n";
          foreach my $aut (split(" and ", $list)) {
              chomp $aut;
              $aut =~ s/^ *//;
              $aut =~ s/ *$//;
#              print "  $aut\n";
              $authors{$aut} += 1;
          }
      }
      foreach my $aut (sort keys %authors) {
         print "$aut -> $authors{$aut}\n";
      }
      print "Author count: ".(scalar keys %authors)."\n";
      print "---------------------------------\n";
      foreach my $aut (sort { $authors{$a} <=> $authors{$b} } keys %authors) {
         print "$aut -> $authors{$aut}\n";
      }
  }
  # my $bibfile = new Text::BibTeX::File("all.bib");

  # count_authors(new Text::BibTeX::File("all.bib"), {"core"=>0,"extern"=>1,"intra"=>1});
  # bibhtml($bibfile,{"core"=>1}, {"article"=>1});
  # bibtable($bibfile);
  # bibtable_bytype($bibfile);

  1;
