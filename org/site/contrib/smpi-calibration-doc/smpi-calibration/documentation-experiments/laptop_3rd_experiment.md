Analysis of Pont-to-point experiments of MPI calls
==================================================

```r
opts_chunk$set(cache=FALSE,dpi=300,echo=FALSE)
```


If needed, you should install the right packages (plyr, ggplot2, and
knitr) with the install.packages command.

```
## Loading required package: plyr
## Loading required package: ggplot2
## Loading required package: methods
## Loading required package: XML
```
Load XML config file and .csv resulting files from the MPI execution



MPI_Send timing
---------------

Timings for this experiment are taken from a ping-pong experiment, used to determine os.

We determine the piecewiese regression based on information taken from the regression file pointed in the XML configuration file


```
##        Limit         Name LimitInf
## 1       4020       Medium        0
## 2      17408 Asynchronous     4020
## 3 2147483647        Large    17408
```
Display the regression factors to help tuning.

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.959e-07 -8.387e-08 -6.053e-08  1.152e-08  7.060e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.700e-07  9.141e-10  295.38   <2e-16 ***
## Size        9.671e-11  9.878e-13   97.91   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.441e-07 on 32219 degrees of freedom
## Multiple R-squared:  0.2293,	Adjusted R-squared:  0.2293 
## F-statistic:  9586 on 1 and 32219 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -6.855e-07 -3.536e-07 -1.724e-07  2.625e-07  2.589e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.695e-06  1.701e-08   99.62   <2e-16 ***
## Size        1.337e-10  1.722e-12   77.63   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 4.963e-07 on 6171 degrees of freedom
## Multiple R-squared:  0.4941,	Adjusted R-squared:  0.494 
## F-statistic:  6027 on 1 and 6171 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value.

The black line representing the regression should be very close to the values, and should drop to 0 when communications use the rendez-vous algorithm (Large messages, with a size > eager_threshold).

If they are not, tune the breakpoints in order to match more closely to your implementation. Thresholds for eager and detached messages depend on the library and the hardware used. Consult the documentation of your library on how to display this information if you can't visually determine it (For Ethernet network we saw values of 65536, while IB networks had values of 12288 or 17408 depending on the implementation)


```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png) 
MPI_Isend timing
---------------

As they may differ from Send times, check this and call it ois, to inject proper timings later.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.996e-07 -1.218e-07 -3.000e-08  1.033e-07  1.437e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 3.081e-07  1.003e-09   307.2   <2e-16 ***
## Size        1.456e-10  1.083e-12   134.5   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.579e-07 on 32139 degrees of freedom
## Multiple R-squared:  0.3601,	Adjusted R-squared:  0.3601 
## F-statistic: 1.809e+04 on 1 and 32139 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.580e-07 -1.790e-07 -1.142e-08  1.508e-07  6.741e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.868e-07  6.392e-09  91.808  < 2e-16 ***
## Size        4.960e-12  6.470e-13   7.665 2.06e-14 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.867e-07 on 6180 degrees of freedom
## Multiple R-squared:  0.009418,	Adjusted R-squared:  0.009258 
## F-statistic: 58.76 on 1 and 6180 DF,  p-value: 2.057e-14
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png) 

MPI_Recv timing
---------------

Timings are used to determine or. This experiment waits for a potentially eager message to arrive before launching the recv for small message size, eliminating waiting times as much as possible.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.481e-07 -4.208e-08 -3.290e-09  3.091e-08  1.487e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.335e-06  4.894e-10  2728.3   <2e-16 ***
## Size        9.533e-11  5.278e-13   180.6   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.695e-08 on 32080 degrees of freedom
## Multiple R-squared:  0.5042,	Adjusted R-squared:  0.5041 
## F-statistic: 3.262e+04 on 1 and 32080 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png) 

Pingpong timing
---------------

pingpong = 2or+2transfer for small messages that are sent
  asynchronously.  For large sizes, communications are synchronous,
  hence we have pingpong = 2transfer.


```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -9.336e-07 -3.636e-07 -1.311e-07  2.278e-07  1.407e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.163e-06  2.800e-09   415.3   <2e-16 ***
## Size        3.995e-10  3.022e-12   132.2   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 4.916e-07 on 39998 degrees of freedom
## Multiple R-squared:  0.3041,	Adjusted R-squared:  0.3041 
## F-statistic: 1.748e+04 on 1 and 39998 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.293e-06 -6.552e-07 -3.738e-07  4.261e-07  1.725e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.748e-06  2.964e-08   92.72   <2e-16 ***
## Size        2.801e-10  3.000e-12   93.37   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 9.658e-07 on 7698 degrees of freedom
## Multiple R-squared:  0.5311,	Adjusted R-squared:  0.531 
## F-statistic:  8719 on 1 and 7698 DF,  p-value: < 2.2e-16
## 
## [1] "----- Large-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.747e-05 -1.743e-06 -7.080e-07  4.650e-07  5.827e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.615e-06  3.738e-08   150.2   <2e-16 ***
## Size        2.056e-10  1.044e-13  1969.1   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.698e-06 on 18498 degrees of freedom
## Multiple R-squared:  0.9953,	Adjusted R-squared:  0.9953 
## F-statistic: 3.877e+06 on 1 and 18498 DF,  p-value: < 2.2e-16
```

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png) 

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning: Removed 24943 rows containing missing values (geom_point).
```

```
## Warning: Removed 30300 rows containing missing values (geom_path).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-2.png) 

Print results in Simgrid's xml format


MPI_Wtime timing
---------------

We made a run with 10 millions calls to MPI\_Wtime and we want to know the time of one call


Time for one MPI_Wtime call

```
## [1] 3.799686e-08
```
MPI_Iprobe timing
----------------
We made 1000 runs of pingpong with pollling on MPI\_Iprobe. Compute the Duration of such a call, and check whether its time is related to the size of the message

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png) 
Time for one MPI_Iprobe call

```
## [1] 4.378468e-07
```

MPI_Test timing
---------------

![plot of chunk unnamed-chunk-19](figure/unnamed-chunk-19-1.png) 
Time for one MPI_Test call

```
## [1] 6.634918e-07
```


Result of calibration.
---------------

The following snippet of XML has to be included at the beginning of your platformfile. Please report to the SimGrid mailing list any bug with the calibration or the generated platform file.


```
## <config id="General">
##  <prop id="smpi/os" value="0:2.69999279911665e-07:9.67131881519866e-11;4020:1.69490988470007e-06:1.33670926926525e-10;17408:0:0"/>
##  <prop id="smpi/ois" value="0:3.08128504038665e-07:1.45641822228952e-10;4020:5.86807108508695e-07:4.95971296094829e-12;17408:0:0"/>
##  <prop id="smpi/or" value="0:1.33516826169969e-06:9.53286878722927e-11;4020:0:0;17408:0:0"/>
##  <prop id="smpi/bw_factor" value="0:0.939303769529411;4020:1.02011981351388;17408:1.38932358995332"/>
##  <prop id="smpi/lat_factor" value="0:-0.00861790097978213;4020:0.137403065044206;17408:0.280742367725014"/>
##  <prop id="smpi/async_small_thres" value="17408"/>
##  <prop id="smpi/send_is_detached_thres" value="17408"/>
##  <prop id="smpi/wtime" value="3.799686e-08"/>
##  <prop id="smpi/iprobe" value="4.37846846846847e-07"/>
##  <prop id="smpi/test" value="6.63491803278689e-07"/>
## </config>
```

```
## [1] "Results written in laptop_3rd_experiment.xml"
```

```
## [1] "laptop_3rd_experiment_output.xml"
```

