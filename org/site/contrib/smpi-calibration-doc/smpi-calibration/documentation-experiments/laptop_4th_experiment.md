Analysis of Pont-to-point experiments of MPI calls
==================================================

```r
opts_chunk$set(cache=FALSE,dpi=300,echo=FALSE)
```


If needed, you should install the right packages (plyr, ggplot2, and
knitr) with the install.packages command.

```
## Loading required package: plyr
## Loading required package: ggplot2
## Loading required package: methods
## Loading required package: XML
```
Load XML config file and .csv resulting files from the MPI execution



MPI_Send timing
---------------

Timings for this experiment are taken from a ping-pong experiment, used to determine os.

We determine the piecewiese regression based on information taken from the regression file pointed in the XML configuration file


```
##        Limit         Name LimitInf
## 1       4020       Medium        0
## 2      17408 Asynchronous     4020
## 3 2147483647        Large    17408
```
Display the regression factors to help tuning.

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -2.521e-07 -4.211e-08 -2.329e-08  1.675e-08  7.729e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.304e-07  5.343e-10   431.3   <2e-16 ***
## Size        9.389e-11  5.776e-13   162.5   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 8.423e-08 on 32243 degrees of freedom
## Multiple R-squared:  0.4504,	Adjusted R-squared:  0.4504 
## F-statistic: 2.642e+04 on 1 and 32243 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -5.219e-07 -2.213e-07 -1.058e-07  9.664e-08  2.592e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.551e-06  1.251e-08   124.0   <2e-16 ***
## Size        1.314e-10  1.266e-12   103.8   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.646e-07 on 6162 degrees of freedom
## Multiple R-squared:  0.6363,	Adjusted R-squared:  0.6363 
## F-statistic: 1.078e+04 on 1 and 6162 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value.

The black line representing the regression should be very close to the values, and should drop to 0 when communications use the rendez-vous algorithm (Large messages, with a size > eager_threshold).

If they are not, tune the breakpoints in order to match more closely to your implementation. Thresholds for eager and detached messages depend on the library and the hardware used. Consult the documentation of your library on how to display this information if you can't visually determine it (For Ethernet network we saw values of 65536, while IB networks had values of 12288 or 17408 depending on the implementation)


```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-6](figure/unnamed-chunk-6-1.png) 
MPI_Isend timing
---------------

As they may differ from Send times, check this and call it ois, to inject proper timings later.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.144e-07 -5.324e-08 -3.394e-08 -1.220e-09  9.133e-07 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.325e-07  6.110e-10  380.47   <2e-16 ***
## Size        5.011e-11  6.597e-13   75.96   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 9.631e-08 on 32233 degrees of freedom
## Multiple R-squared:  0.1518,	Adjusted R-squared:  0.1518 
## F-statistic:  5770 on 1 and 32233 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.380e-07 -8.485e-08 -5.580e-08  1.887e-08  1.254e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 4.829e-07  4.616e-09  104.61   <2e-16 ***
## Size        6.659e-12  4.672e-13   14.25   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 1.348e-07 on 6178 degrees of freedom
## Multiple R-squared:  0.03183,	Adjusted R-squared:  0.03167 
## F-statistic: 203.1 on 1 and 6178 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-9](figure/unnamed-chunk-9-1.png) 

MPI_Recv timing
---------------

Timings are used to determine or. This experiment waits for a potentially eager message to arrive before launching the recv for small message size, eliminating waiting times as much as possible.


Display the regression factors to help tuning

```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -3.172e-07 -1.760e-07 -1.000e-07  7.000e-08  7.168e-06 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.273e-06  2.006e-09  634.63   <2e-16 ***
## Size        1.162e-10  2.166e-12   53.63   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.156e-07 on 32129 degrees of freedom
## Multiple R-squared:  0.08218,	Adjusted R-squared:  0.08215 
## F-statistic:  2877 on 1 and 32129 DF,  p-value: < 2.2e-16
```
Visual representation of the computed data, to visualize correctness of the computed value

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-12](figure/unnamed-chunk-12-1.png) 

Pingpong timing
---------------

pingpong = 2or+2transfer for small messages that are sent
  asynchronously.  For large sizes, communications are synchronous,
  hence we have pingpong = 2transfer.


```
## [1] "----- Medium-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -7.189e-07 -2.474e-07 -1.023e-07  1.213e-07  1.247e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 1.036e-06  2.194e-09   471.9   <2e-16 ***
## Size        3.862e-10  2.368e-12   163.1   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.853e-07 on 39998 degrees of freedom
## Multiple R-squared:  0.3993,	Adjusted R-squared:  0.3993 
## F-statistic: 2.659e+04 on 1 and 39998 DF,  p-value: < 2.2e-16
## 
## [1] "----- Asynchronous-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -9.977e-07 -4.131e-07 -2.266e-07  1.421e-07  2.577e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 2.552e-06  2.365e-08   107.9   <2e-16 ***
## Size        2.702e-10  2.394e-12   112.9   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 7.707e-07 on 7698 degrees of freedom
## Multiple R-squared:  0.6233,	Adjusted R-squared:  0.6233 
## F-statistic: 1.274e+04 on 1 and 7698 DF,  p-value: < 2.2e-16
## 
## [1] "----- Large-----"
## 
## Call:
## lm(formula = Duration ~ Size, data = d[d$group == bp[bp$Limit == 
##     lim, ]$Name, ])
## 
## Residuals:
##        Min         1Q     Median         3Q        Max 
## -1.562e-05 -1.595e-06 -3.660e-07  6.180e-07  6.987e-05 
## 
## Coefficients:
##              Estimate Std. Error t value Pr(>|t|)    
## (Intercept) 5.050e-06  3.655e-08   138.2   <2e-16 ***
## Size        2.107e-10  1.021e-13  2063.9   <2e-16 ***
## ---
## Signif. codes:  0 '***' 0.001 '**' 0.01 '*' 0.05 '.' 0.1 ' ' 1
## 
## Residual standard error: 3.616e-06 on 18498 degrees of freedom
## Multiple R-squared:  0.9957,	Adjusted R-squared:  0.9957 
## F-statistic: 4.26e+06 on 1 and 18498 DF,  p-value: < 2.2e-16
```

```
## Saving 7 x 7 in image
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

```
## Warning: Removed 1 rows containing missing values (geom_segment).
```

```
## Warning: Removed 1 rows containing missing values (geom_text).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-1.png) 

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning in scale$trans$trans(x): NaNs produced
```

```
## Warning: Removed 28531 rows containing missing values (geom_point).
```

```
## Warning: Removed 33800 rows containing missing values (geom_path).
```

![plot of chunk unnamed-chunk-13](figure/unnamed-chunk-13-2.png) 

Print results in Simgrid's xml format


MPI_Wtime timing
---------------

We made a run with 10 millions calls to MPI\_Wtime and we want to know the time of one call


Time for one MPI_Wtime call

```
## [1] 3.455213e-08
```
MPI_Iprobe timing
----------------
We made 1000 runs of pingpong with pollling on MPI\_Iprobe. Compute the Duration of such a call, and check whether its time is related to the size of the message

![plot of chunk unnamed-chunk-17](figure/unnamed-chunk-17-1.png) 
Time for one MPI_Iprobe call

```
## [1] 4.163727e-07
```

MPI_Test timing
---------------

![plot of chunk unnamed-chunk-19](figure/unnamed-chunk-19-1.png) 
Time for one MPI_Test call

```
## [1] 5.948594e-07
```


Result of calibration.
---------------

The following snippet of XML has to be included at the beginning of your platformfile. Please report to the SimGrid mailing list any bug with the calibration or the generated platform file.


```
## <config id="General">
##  <prop id="smpi/os" value="0:2.30428178692274e-07:9.38918859247977e-11;4020:1.5508198700576e-06:1.31439681680103e-10;17408:0:0"/>
##  <prop id="smpi/ois" value="0:2.32488033325077e-07:5.01131698959389e-11;4020:4.82909861377129e-07:6.65892654730719e-12;17408:0:0"/>
##  <prop id="smpi/or" value="0:1.2728963928619e-06:1.16155532228468e-10;4020:0:0;17408:0:0"/>
##  <prop id="smpi/bw_factor" value="0:1.05816254755788;4020:1.05751555288645;17408:1.35582984655202"/>
##  <prop id="smpi/lat_factor" value="0:-0.0118634518918262;4020:0.127585482259507;17408:0.252478555742"/>
##  <prop id="smpi/async_small_thres" value="17408"/>
##  <prop id="smpi/send_is_detached_thres" value="17408"/>
##  <prop id="smpi/wtime" value="3.455213e-08"/>
##  <prop id="smpi/iprobe" value="4.16372727272727e-07"/>
##  <prop id="smpi/test" value="5.94859375e-07"/>
## </config>
```

```
## [1] "Results written in laptop_4th_experiment.xml"
```

```
## [1] "laptop_4th_experiment_output.xml"
```

