# -*- coding: utf-8 -*-
#+SETUPFILE: ../../org-templates/level-1.org
#+INCLUDE:   ../../org-templates/level-1.org
#+OPTIONS:   num:2
#+STARTUP: overview indent inlineimages
#+AUTHOR: Augustin Degomme, Christian Heinrich, Arnaud Legrand

#+HTML: <h1>
  Welcome to SMPI Calibration's documentation!
#+HTML: </h1>

#+TOC: headlines 2

#+BEGIN_CENTER
*This page is deprecated. Please see
https://framagit.org/simgrid/platform-calibration/.*
#+END_CENTER

We are currently (since 2013... :() trying to strengthen and automate this
procedure but we still lack time and manpower.

