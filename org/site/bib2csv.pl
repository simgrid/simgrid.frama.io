#! /usr/bin/perl -w

use strict;
my $input ="all.bib";
my $output ="all.csv";
my $line;
open(INPUT,$input) or die;
open(OUTPUT,">".$output) or die;
my($type,$key,$year,$category,$title);
my $issue = 0;
my ($use,$extend,$citeonly,$todo) = (0,0,0,0);
my $firstime = 1;

my $output_title=1; # Set it to 1 to display all titles in a file, to find dupplicates

open(TITLES, ">titles.txt") or die if ($output_title);

print OUTPUT "Type,Category,Year\n";
while(defined($line=<INPUT>)) {
    chomp $line;
    if($line =~ /^@/) {
	# Start of a new entry. Do some checkup, display the previous entry and start building the next one

        if(!defined($category) && $firstime!=1) {
            print STDERR "Entry without category: $key|$title $type $year\n";
            $issue++;
        }
        if(!defined($type) && defined($title)) {
            print STDERR "Entry without type: $key $title\n";
        }
        if (defined($category) && ($category eq "use" || $category eq "extend")) {
            if (!defined($title)) {
                print STDERR "Entry without title: $key\n";
            }
            if(!defined($year) && $firstime!=1) {
                print STDERR "Entry without year: $key|$title $type $category $year\n";
                $issue++;
            }
            if(defined($type)) {
                $title =~ s/[-{},.; _]//g;
                print TITLES lc($title)."\n" if ($output_title);
                print OUTPUT "$type,$category,$year\n";
                undef $type;
                undef $year;
                undef $category;
                undef $title;
                undef $key;
            }
        }
        $line =~ m/@([^{]*)[{](.*)/;
        ($type,$key) = ($1,$2);
        $type = lc($type);
        # print "$type\n";
        $firstime=0;
    }

    if($line =~ /\s*[^n]?note\s*=\s*[{"](.+)["}]/i) {
        $category = $1;
        if ($category eq "use") {
            $use++;
        } elsif ($category eq "citeonly") {
            $citeonly++;
        } elsif ($category eq "extend") {
            $extend++;
        } elsif ($category eq "TODO") {
	    $todo++;
	} else {
            print STDERR "Invalid category: $category $key\n";
        }
    }
    $year = $1  if ($line =~ /year\s*=\s*[{"]?(\d+)["}]?/i);
    $title = $1 if ($line =~ /^\s*[^k]?title\s*=\s*[{"](.*)["}]?\s*,?\s*$/i);
}

# Don't forget to output the last entry	
print OUTPUT "$type,$category,$year\n" if (defined($type) && defined($category) && defined($year) && ($category eq "use" || $category eq "extend"));

print STDERR "use:$use extend:$extend total:".($use+$extend)." citeonly:$citeonly todo:$todo\n";
print STDERR "$issue issue(s)\n";

# Print all papers that are dupplicates
if ($output_title) {
    # White-listing a paper that has the same title in the workshop and journal version.
    print qx,sort titles.txt | uniq -c | grep -v ' 1 '|grep -v teachingparallelanddistributedcomputingconceptsinsimulationwithwrench,;
}
